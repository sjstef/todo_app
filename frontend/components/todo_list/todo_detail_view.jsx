import React from 'react';
import StepForm from './step_form';

const StepList = ({ deleteStep, steps }) => {
  const stepLis = steps.map((step) => {
    return (
      <li key={step.id}>
        {step.title} - {step.description}<br></br>
        <button onClick={ () => deleteStep(step) }>Delete</button>
      </li>
    );
  });
  return (
    <ul>
      {stepLis}
    </ul>
  );
};

class TodoDetailView extends React.Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {
  }

  render () {
    return (
      <div>
          {this.props.todo.body}<br></br>
        <ul>
          <StepList deleteStep={this.props.deleteStep} steps={this.props.steps} />
        </ul>
        <br></br>
        <StepForm createStep={ this.props.createStep } todoId={this.props.todoId} />
        <br></br>
        <button onClick={ () => this.props.deleteTodo(this.props.todo) }>Delete!</button>
      </div>
    );
  }
}

export default TodoDetailView;
