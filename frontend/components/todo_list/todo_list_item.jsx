import React from 'react';
import TodoDetailViewContainer from './todo_detail_view_container';


export default class TodoListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.props);
    this.state.detail = false;
    this.updateTodo = this.props.updateTodo.bind(this);
  }

  render() {
    return (
        <li>
          <a onClick={() => this.setState({detail: !this.state.detail})}>{this.props.todo.title}</a>
          <button onClick={ this.toggleDone.bind(this) } >
            {this.props.todo.done ? "Undo" : "Done"}
          </button>
          {this.state.detail ? <TodoDetailViewContainer todoId={this.props.todo.id}/> : ""}
        </li>
      );
  }

  toggleDone () {
    this.updateTodo(Object.assign({}, this.props.todo, {done: !this.props.todo.done}));
  }

}
