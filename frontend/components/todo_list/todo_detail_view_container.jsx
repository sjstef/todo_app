import { connect } from 'react-redux';
import TodoDetailView from './todo_detail_view';
import { deleteTodo } from '../../actions/todo_actions';
import { createStep, updateStep, deleteStep } from '../../actions/step_actions';
import { allTodos, stepsByTodoId } from '../../reducers/selectors';

const mapStateToProps = (state, ownProps) => {
  return {
    todo: state.todos[ownProps.todoId],
    steps: stepsByTodoId(state, ownProps.todoId),
    errors: state.errors
  };
};

const mapDispatchToProps = (dispatch) => {
  return ({
    deleteTodo: (todo) => dispatch(deleteTodo(todo)),
    deleteStep: (step) => dispatch(deleteStep(step)),
    updateStep: (step) => dispatch(updateStep(step)),
    createStep: (step) => dispatch(createStep(step))
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoDetailView);
