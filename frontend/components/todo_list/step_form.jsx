import React from 'react';
import ReactDOM from 'react-dom';
import { uniqueId } from '../../util/util';

export default class StepForm extends React.Component {
    constructor(props){
      super(props);
      this.state = { title: "", description: "" };
      this.state.todoId = props.todoId;
      this.handleTitleChange = this.handleTitleChange.bind(this);
      this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleTitleChange (e) {
      this.setState({ title: e.target.value });
    }

    handleDescriptionChange (e) {
      this.setState({ description: e.target.value });
    }

    handleSubmit (e) {
      e.preventDefault();
      const newStep = { title: this.state.title,
        description: this.state.description, done: false, todoId: this.state.todoId};
      this.props.createStep(newStep).then(() => {
        this.setState({title: '', description: ''});
      });
    }

    render() {
      return (
        <form onSubmit={ this.handleSubmit }>
          <label>Title:</label>
          <br></br>
          <input type="text" onChange={ this.handleTitleChange }
            value={ this.state.title } />
          <br></br>
          <label>Description:</label>
          <br></br>
          <textarea onChange={ this.handleDescriptionChange }
            value={ this.state.description } >
          </textarea>
          <br></br>
          <button>Create Step!</button>
        </form>
      );
    }
}
