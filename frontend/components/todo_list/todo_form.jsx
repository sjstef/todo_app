import React from 'react';
import ReactDOM from 'react-dom';
import { uniqueId } from '../../util/util';

export default class TodoForm extends React.Component {
    constructor(props){
      super(props);
      this.state = { title: "", body: "" };
      this.handleTitleChange = this.handleTitleChange.bind(this);
      this.handleBodyChange = this.handleBodyChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleTitleChange (e) {
      this.setState({ title: e.target.value });
    }

    handleBodyChange (e) {
      this.setState({ body: e.target.value });
    }

    handleSubmit (e) {
      e.preventDefault();
      const newTodo = ({ id: uniqueId(), title: this.state.title,
        body: this.state.body, done: false });
      this.props.createTodo(newTodo).then(() => {
        this.setState({title: '', body: ''});
      });
    }

    render() {
      return (
        <form onSubmit={ this.handleSubmit }>
          <label>Title:</label>
          <br></br>
          <input type="text" onChange={ this.handleTitleChange }
            value={ this.state.title } />
          <br></br>
          <label>Body:</label>
          <br></br>
          <textarea onChange={ this.handleBodyChange }
            value={ this.state.body } >
          </textarea>
          <br></br>
          <button>Create Todo!</button>
          <p>{ this.props.errors ? this.props.errors[0] : ""}</p>
        </form>
      );
    }
}
