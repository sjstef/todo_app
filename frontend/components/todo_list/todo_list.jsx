import React from 'react';
import TodoForm from './todo_form';
import TodoListItem from './todo_list_item';

class TodoList extends React.Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {
    this.props.fetchTodos();
    this.props.fetchSteps();
  }

  render() {
    const todoLis = [];
    this.props.todos.forEach((todo) => {
      todoLis.push(<TodoListItem key={todo.id} todo={todo}
        removeTodo={this.props.removeTodo}
        updateTodo={this.props.updateTodo}
      />);
    });
    return (
      <div>
        <ul>
          {todoLis}
        </ul>
        <TodoForm createTodo={ this.props.createTodo } errors={ this.props.errors } />
      </div>
    );
  }
}

export default TodoList;
