import { RECEIVE_TODOS, RECEIVE_TODO, REMOVE_TODO } from "../actions/todo_actions";

const _defaultState = {
  todos: {},
};

const initialState = {
  1: {
    id: 1,
    title: 'wash car',
    body: 'with soap',
    done: false
  },
  2: {
    id: 2,
    title: 'wash dog',
    body: 'with shampoo',
    done: true
  },
};

const todosReducer = (state = initialState, action) => {
  switch(action.type) {
    case RECEIVE_TODOS: {
        const newState = {};
        action.todos.forEach((todo) => {
          newState[todo.id] = todo;
        });
        return newState;
      }
    case RECEIVE_TODO: {
      let newTodos = Object.assign({}, state);
      newTodos[action.todo.id] = action.todo;
      return newTodos;
    }
    case REMOVE_TODO: {
      const newState = Object.assign({}, state);
      delete newState[action.todo.id];
      return newState;
    }
    default:
      return state;
  }
};

export default todosReducer;
