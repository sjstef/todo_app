import { RECEIVE_ERRORS, CLEAR_ERRORS } from "../actions/error_actions";

const _defaultState = [];

const errorsReducer = (state = _defaultState, action) => {
  switch(action.type) {
    case RECEIVE_ERRORS: {
        return action.errors;
      }
    case CLEAR_ERRORS: {
      return [];
    }
    default:
      return state;
  }
};

export default errorsReducer;
