import { RECEIVE_STEPS, RECEIVE_STEP, REMOVE_STEP } from "../actions/step_actions";

const _defaultState = {};

const initialState = {
    1: { // this is the step with id = 1
      id: 1,
      title: 'walk to store',
      done: false,
      todo_id: 1
    },
    2: { // this is the step with id = 2
      id: 2,
      title: 'buy soap',
      done: false,
      todo_id: 1
    }
  };

const stepsReducer = (state = initialState, action) => {
  switch(action.type) {
    case RECEIVE_STEPS: {
        const newState = {};
        action.steps.forEach((step) => {
          newState[step.id] = step;
        });
        return newState;
      }
    case RECEIVE_STEP: {
      let newTodos = Object.assign({}, state);
      newTodos[action.step.id] = action.step;
      return newTodos;
    }
    case REMOVE_STEP: {
      const newState = Object.assign({}, state);
      delete newState[action.step.id];
      return newState;
    }
    default:
      return state;
  }
};

export default stepsReducer;
