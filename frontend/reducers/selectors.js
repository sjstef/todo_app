

export const allTodos = (state) => {
  let keys = Object.keys(state.todos);
  return keys.map((key) => Object.assign({}, state.todos[key]));
};

export const stepsByTodoId = (state, todoId) => {
  return allSteps(state).filter((step) => step.todo_id === todoId);
};

const allSteps = (state) => {
  let keys = Object.keys(state.steps);
  return keys.map((key) => Object.assign({}, state.steps[key]));
};
