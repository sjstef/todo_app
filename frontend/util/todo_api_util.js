export const getTodos = () => {
  return $.ajax('api/todos');
};

export const createTodo = (todo) => {
  return $.ajax({
    url: "api/todos",
    method: "POST",
    data: {todo},
  });
};

export const updateTodo = (todo) => {
  return $.ajax({
    url: `api/todos/${todo.id}`,
    method: "PATCH",
    data: {todo},
  });
};

export const deleteTodo = (todo) => {
  return $.ajax({
    url: `api/todos/${todo.id}`,
    method: "DELETE",
    data: {todo},
  });
};
