export const getSteps = () => {
  return $.ajax('api/steps');
};

export const createStep = (step) => {
  return $.ajax({
    url: `api/steps/`,
    method: "POST",
    data: { step, todo_id: step.todoId },
  });
};

export const updateStep = (step) => {
  return $.ajax({
    url: `api/steps/${step.id}`,
    method: "PATCH",
    data: { step, todo_id: step.todoId },
  });
};

export const deleteStep = (step) => {
  return $.ajax({
    url: `api/steps/${step.id}`,
    method: "DELETE",
    data: {step},
  });
};
