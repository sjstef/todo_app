import React from 'react';
import ReactDOM from 'react-dom';
import store from './store/store';
import { receiveTodos, receiveTodo, fetchTodos } from './actions/todo_actions';
import { App } from './components/app.jsx';
import { Root } from './components/root.jsx';
import { allTodos } from './reducers/selectors.js';
import { getTodos } from './util/todo_api_util';

document.addEventListener('DOMContentLoaded', () => {
  window.store = store;
  window.allTodos = allTodos;
  window.fetchTodos = fetchTodos;
  window.getTodos = getTodos;
  window.receiveTodo = receiveTodo;
  window.receiveTodos = receiveTodos;
  ReactDOM.render(<Root store={ store }/>, document.getElementById('content'));
});
