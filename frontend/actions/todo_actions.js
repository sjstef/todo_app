import * as APIUtil from '../util/todo_api_util';
import { receiveErrors, clearErrors } from './error_actions';

export const RECEIVE_TODOS = 'RECEIVE_TODOS';
export const RECEIVE_TODO = 'RECEIVE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';

export const receiveTodos = (todos) => {
  return ({ type: RECEIVE_TODOS, todos });
};

export const receiveTodo = (todo) => {
  return ({ type: RECEIVE_TODO, todo });
};

export const removeTodo = (todo) => {
  return ({ type: REMOVE_TODO, todo });
};

export const fetchTodos = () => (dispatch) => {
  return APIUtil.getTodos().then((todos) => dispatch(receiveTodos(todos)));
};

export const createTodo = (todo) => (dispatch) => {
  return APIUtil.createTodo(todo).then(
    todo => {
      dispatch(clearErrors());
      dispatch(receiveTodo(todo));
    },
    err => dispatch(receiveErrors(err.responseJSON))
  );
};

export const updateTodo = (todo) => (dispatch) => {
  return APIUtil.updateTodo(todo).then(
    todo => {
      dispatch(clearErrors());
      dispatch(receiveTodo(todo));
    },
    err => dispatch(receiveErrors(err.responseJSON))
  );
};

export const deleteTodo = (todo) => (dispatch) => {
  return APIUtil.deleteTodo(todo).then(
    todo => {
      dispatch(clearErrors());
      dispatch(removeTodo(todo));
    },
    err => dispatch(receiveErrors(err.responseJSON))
  );
};
